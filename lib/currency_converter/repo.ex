defmodule CurrencyConverter.Repo do
  @moduledoc false

  use Ecto.Repo,
    otp_app: :currency_converter,
    adapter: Ecto.Adapters.Postgres
end
