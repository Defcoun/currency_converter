defmodule CurrencyConverter.Schema.CurrencyHistory do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias CurrencyConverter.Schema.CurrencyHistory

  schema "currency_history" do
    field(:amount, :float)
    field(:currency, :string)
    field(:date, :date)
    field(:result, :float)

    timestamps()
  end

  @doc false
  def changeset(%CurrencyHistory{} = item, attrs) do
    item
    |> cast(attrs, [
      :amount,
      :currency,
      :date,
      :result
    ])
    |> validate_required([
      :amount,
      :currency,
      :date,
      :result
    ])
    |> unique_constraint([:date, :currency, :amount])
  end
end
