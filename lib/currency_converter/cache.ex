defmodule CurrencyConverter.Cache do
  @moduledoc """
  Модуль для кеширования курс валют за определенную дату.
  Срок жизни записи 3 секунды
  """

  use GenServer
  # @active true
  # Кеш действителен в течении 3 секунд
  @default_ttl 3

  @doc false

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil, name: __MODULE__)
  end

  @doc false

  @impl true
  def init(_) do
    table = :ets.new(__MODULE__, [])
    {:ok, table}
  end

  @doc false

  @impl true
  def handle_call({:get, id}, _, table) do
    value =
      :ets.lookup(table, id)
      |> List.first()
      |> case do
        {^id, value, ts} -> [value, ts]
        _ -> nil
      end

    {:reply, value, table}
  end

  @doc false

  @impl true
  def handle_cast({:set, id, value, ts}, table) do
    :ets.insert(table, {id, value, ts})

    {:noreply, table}
  end

  @doc false

  @impl true
  def handle_cast({:delete, id}, table) do
    :ets.delete(table, id)

    {:noreply, table}
  end

  @doc """
  Функция для записи курса валюты за конкретную дату

  Атрибуты

    - currency: буквенное обозначение кода валюты. Пример: USD, EUR, KZN
    - date: за указанную дату будет взят курс валюты по отношению к рублю
    - value: значение курса валют

  ## Примеры

      iex> CurrencyConverter.Cache.set("USD", ~D[2021-06-04], 73.2636)
      :ok

  """
  @spec set(currency :: String.t(), date :: Date.t(), value :: float()) ::
          {:ok, boolean()} | {:error, atom()}
  def set(currency, date, value) do
    case get_id(currency, date) do
      {:ok, id} ->
        GenServer.cast(__MODULE__, {:set, id, value, timestamp()})

      reason ->
        reason
    end
  end

  @doc """
  Функция для получений курса валюты за конкретную дату из Кеша с проверкой срока жизни записи

  Атрибуты

    - currency: буквенное обозначение кода валюты. Пример: USD, EUR, KZN
    - date: за указанную дату будет взят курс валюты по отношению к рублю
    - ttl: срок жизни записи

  ## Примеры
      iex(2)> CurrencyConverter.Cache.get("USD", ~D[2021-06-04], 3)
      {:ok, 73.2636}

      iex(3)> CurrencyConverter.Cache.get("USD", ~D[2040-01-01], 3)
      {:error, :not_value}
  """
  @spec get(currency :: String.t(), date :: Date.t(), ttl :: integer()) ::
          {:ok, float()} | {:error, atom()}
  def get(currency, date, ttl \\ @default_ttl) do
    case get_id(currency, date) do
      {:ok, id} ->
        case GenServer.call(__MODULE__, {:get, id}) do
          nil ->
            {:error, :not_value}

          [value, ts] ->
            case timestamp() - ts <= ttl do
              true -> {:ok, value}
              _ -> {:error, :not_value}
            end
        end

      reason ->
        reason
    end
  end

  @doc """
  Удаляем старую запись в кеше
  Надо написать процесс, который будет получать все значения в кеше и удалять старые
  через Enum.each -> spawn(__MODULE__, :delete, [id])

  Функция пока не используется
  """
  def delete(id) do
    GenServer.cast(__MODULE__, {:delete, id})
  end

  @doc false
  @spec get_id(currency :: String.t(), date :: Date.t()) ::
          {:ok, String.t()} | {:error, atom()}
  defp get_id(currency, date) do
    case Timex.format(date, "{YYYY}{0M}{0D}") do
      {:ok, date} -> {:ok, "#{currency}_#{date}"}
      _ -> {:error, :error_currency_key}
    end
  end

  @doc false
  @spec timestamp() :: integer() | {:error, atom()}
  defp timestamp, do: DateTime.to_unix(DateTime.utc_now())
end
