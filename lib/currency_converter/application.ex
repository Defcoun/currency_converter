defmodule CurrencyConverter.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      CurrencyConverter.Repo,
      CurrencyConverter.Cache
    ]

    opts = [strategy: :one_for_one, name: CurrencyConverter.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
