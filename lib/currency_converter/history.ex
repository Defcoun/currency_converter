defmodule CurrencyConverter.History do
  @moduledoc """
  Модуль используется для работы с историческими данными, хранящиеся в БД
  """
  import Ecto.Query, warn: false
  alias CurrencyConverter.Repo
  alias CurrencyConverter.Schema.CurrencyHistory

  if Mix.env() in [:dev, :test] do
    @doc """
    Очищаем БД
    """

    @spec truncate(atom()) :: :ok
    def truncate(schema) do
      table_name = schema.__schema__(:source)
      Repo.query("TRUNCATE #{table_name}", [])
      :ok
    end
  end

  @doc """
  Создание записи расчета курса рубля

  ## Атрибуты

    - attrs: %{amount: float(), currency: String.t(), date: Date.t(), result: float()}

  ## Примеры

      iex> CurrencyConverter.History.create(%{amount: 10, currency: "USD", date: ~D[2021-06-04], result: 732.636})
      {:ok, true}


  """
  @spec create(attrs :: map()) :: {:ok, boolean()} | {:error, atom()}

  def create(attrs) do
    %CurrencyHistory{}
    |> CurrencyHistory.changeset(attrs)
    |> Repo.insert()
    |> case do
      {:ok, _} -> {:ok, true}
      _ -> {:error, :error_incert_history}
    end
  end

  @doc """
  Получение записи расчета курса рубля за определенную дату для определенное количество конкретной валюты

  ## Атрибуты

    - amount: количество валюты
    - currency_iso: буквенное обозначение кода валюты. Пример: USD, EUR, KZN
    - date: за указанную дату будет взят курс валюты по отношению к рублю

  ## Примеры

      iex> CurrencyConverter.History.get(10, "USD", ~D[2021-06-04])
      {:ok, 732.636}

      iex> CurrencyConverter.History.get(10, "USD", ~D[2022-06-04])
      {:error, :not_history}

  """
  @spec get(amount :: float(), currency_iso :: String.t(), date :: Date.t()) ::
          {:ok, float()} | {:error, atom()}

  def get(amount, currency, date) do
    case Repo.get_by(CurrencyHistory, amount: amount, currency: currency, date: date) do
      %{result: result} -> {:ok, result}
      _ -> {:error, :not_history}
    end
  end
end
