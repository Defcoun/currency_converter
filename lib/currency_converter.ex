defmodule CurrencyConverter do
  @moduledoc """
  Модуль для конвертации валюты.
  Это тестовое неоплачиваемое задание,поэтому я выполнил его для демонстрации умения работать с некоторыми модулями Elixir.

  """
  alias CurrencyConverter.{
    History,
    Cache
  }

  @doc """
  Функция предназначенную для конвертации разных валют в рубли.

  ## Атрибуты

    - amount: количество валюты
    - currency_iso: буквенное обозначение кода валюты. Пример: USD, EUR, KZN
    - date: за указанную дату будет взят курс валюты по отношению к рублю

  ## Примеры

      iex> CurrencyConverter.convert(10, "USD", ~D[2021-06-04])
      {:ok, 732.636}

      iex> CurrencyConverter.convert(:error, "USD", ~D[2021-06-04])
      {:error, :incorrect_amount}

      iex> CurrencyConverter.convert(10, :error, ~D[2021-06-04])
      {:error, :incorrect_currency}

      iex> CurrencyConverter.convert(10, "USD", :error)
      {:error, :incorrect_date}

  """

  @spec convert(amount :: float(), currency_iso :: String.t(), date :: Date.t()) ::
          {:ok, float()} | {:error, atom()}

  def convert(amount, currency_iso, date) do
    with {:ok, amount} <- check_amount(amount),
         {:ok, date} <- check_date(date),
         {:ok, currency_iso} <- check_currency(currency_iso) do
      case History.get(amount, currency_iso, date) do
        {:ok, result} ->
          {:ok, result}

        _ ->
          case Cache.get(currency_iso, date) do
            {:ok, rate} ->
              calculate_rate(amount, rate, currency_iso, date)

            _ ->
              case get_sbr_daily(date) do
                {:ok, currencies} ->
                  case currencies do
                    %{^currency_iso => rate} -> calculate_rate(amount, rate, currency_iso, date)
                    _ -> {:error, :not_found_currency}
                  end

                reason ->
                  reason
              end
          end
      end
    end
  end

  @doc """
  Получаем данные курс валют за конкретную дату со сбербанка.
  """

  @spec get_sbr_daily(date :: Date.t(), count :: integer()) :: {:ok, map()} | {:error, atom()}
  def get_sbr_daily(date, count \\ 1) do
    case Timex.is_valid?(date) && count < 14 do
      true ->
        #        date = Timex.shift(date, days: 1)
        year = date.year
        month = Timex.format!(date, "{0M}")
        day = Timex.format!(date, "{0D}")

        "https://www.cbr-xml-daily.ru/archive/#{year}/#{month}/#{day}/daily_json.js"
        |> Tesla.get()
        |> case do
          {:ok, %Tesla.Env{status: 200, body: body}} ->
            case Jason.decode!(body) do
              %{"Valute" => currencies} ->
                currencies =
                  currencies
                  |> Map.to_list()
                  |> Enum.map(fn {currency, %{"Nominal" => nominal, "Value" => value}} ->
                    {currency, Float.round(value / nominal, 4)}
                  end)
                  |> Map.new()

                # Записываем Cache в фоне
                spawn(__MODULE__, :set_currencies_to_cache, [currencies, date])

                {:ok, currencies}

              _ ->
                {:error, :error_get_sbr_daily}
            end

          {:ok, %Tesla.Env{status: 404, body: body}} ->
            case Jason.decode!(body) do
              %{"error" => "Not found"} ->
                get_sbr_daily(date, count + 1)

              _ ->
                {:error, :error_get_sbr_daily}
            end

          _ ->
            {:error, :error_get_sbr_daily}
        end

      _ ->
        {:error, :incorrect_date}
    end
  end

  @doc false
  @spec set_currencies_to_cache(currencies :: map(), date :: Date.t()) :: :ok | {:error, atom()}
  def set_currencies_to_cache(currencies, date) do
    currencies
    |> Map.to_list()
    |> Enum.each(fn {currency, value} ->
      CurrencyConverter.Cache.set(currency, date, value)
    end)
  end

  @doc false
  @spec check_amount(amount :: any()) :: {:ok, float()} | {:error, atom()}
  defp check_amount(amount) do
    amount =
      cond do
        is_binary(amount) ->
          amount
          |> Float.parse()
          |> case do
            {value, _} -> value
            _ -> 0
          end

        is_integer(amount) ->
          amount

        is_float(amount) ->
          amount

        true ->
          0
      end

    case amount > 0 do
      true -> {:ok, amount}
      _ -> {:error, :incorrect_amount}
    end
  end

  @doc false
  @spec check_date(date :: any()) :: {:ok, Date.t()} | {:error, atom()}
  defp check_date(date) do
    case Timex.is_valid?(date) do
      true -> {:ok, Timex.to_date(date)}
      _ -> {:error, :incorrect_date}
    end
  end

  @doc false
  @spec check_currency(currency :: String.t()) :: {:ok, String.t()} | {:error, atom()}
  defp check_currency(currency) do
    case is_binary(currency) do
      true ->
        currency =
          currency
          |> String.trim()
          |> String.slice(0..2)
          |> String.upcase()

        {:ok, currency}

      _ ->
        {:error, :incorrect_currency}
    end
  end

  @doc false
  @spec calculate_rate(
          amount :: float(),
          rate :: float(),
          currency :: String.t(),
          date :: Date.t()
        ) ::
          {:ok, float} | {:error, atom()}
  defp calculate_rate(amount, rate, currency, date) do
    result = Float.round(amount * rate, 4)

    # Записываем результат в БД в фоне
    attrs = %{
      amount: amount,
      currency: currency,
      date: date,
      result: result
    }

    spawn(CurrencyConverter.History, :create, [attrs])

    {:ok, result}
  end
end
