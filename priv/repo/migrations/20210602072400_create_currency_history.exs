defmodule CurrencyConverter.Repo.Migrations.History do
  use Ecto.Migration

  def change do
    create table(:currency_history) do
      add(:amount, :float)
      add(:currency, :string)
      add(:date, :date)
      add(:result, :float)

      timestamps()
    end

    create(index(:currency_history, [:date, :currency]))
    create unique_index(:currency_history, [:date, :currency, :amount])
  end
end
