use Mix.Config

config :tesla, adapter: Tesla.Adapter.Hackney
config :currency_converter, ecto_repos: [CurrencyConverter.Repo]

import_config "#{Mix.env()}.exs"
