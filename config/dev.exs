use Mix.Config

config :currency_converter, CurrencyConverter.Repo,
  username: "postgres",
  password: "postgres",
  database: "currency_converter",
  hostname: "localhost",
  pool_size: 10
