defmodule CurrencyConverterTest do
  use ExUnit.Case, async: false

  # Тесты встроенные в документацию
  doctest CurrencyConverter
  #  doctest CurrencyConverter.History
  #  doctest CurrencyConverter.Cache

  test "set and get cache" do
    assert CurrencyConverter.Cache.set("USD", ~D[2021-06-04], 73.2636) == :ok
    assert CurrencyConverter.Cache.get("USD", ~D[2021-06-04], 3) == {:ok, 73.2636}
    assert CurrencyConverter.Cache.get("USD", ~D[2040-01-01], 3) == {:error, :not_value}
  end

  test "inset and get history" do
    CurrencyConverter.History.truncate(CurrencyConverter.Schema.CurrencyHistory)

    assert CurrencyConverter.History.create(%{
             amount: 10,
             currency: "USD",
             date: ~D[2021-06-04],
             result: 732.636
           }) == {:ok, true}

    assert CurrencyConverter.History.get(10, "USD", ~D[2021-06-04]) == {:ok, 732.636}
    assert CurrencyConverter.History.get(10, "USD", ~D[2022-06-04]) == {:error, :not_history}
  end

  test "get currencies from cbr" do
    assert CurrencyConverter.get_sbr_daily(~D[2021-06-04]) ==
             {:ok,
              %{
                "AUD" => 56.6328,
                "CZK" => 3.5058,
                "UAH" => 2.68,
                "HUF" => 0.2576,
                "INR" => 1.0029,
                "DKK" => 12.0061,
                "NOK" => 8.8054,
                "SEK" => 8.8473,
                "KZT" => 0.1711,
                "TMT" => 20.9624,
                "TJS" => 6.4238,
                "BRL" => 14.4333,
                "AMD" => 0.1407,
                "MDL" => 4.1509,
                "KRW" => 0.0658,
                "CNY" => 11.4589,
                "SGD" => 55.3392,
                "BGN" => 45.6499,
                "EUR" => 89.2644,
                "XDR" => 105.6703,
                "TRY" => 8.4993,
                "ZAR" => 5.4168,
                "CHF" => 81.395,
                "KGS" => 0.867,
                "GBP" => 103.8072,
                "UZS" => 0.0069,
                "JPY" => 0.6673,
                "USD" => 73.2636,
                "PLN" => 20.0103,
                "RON" => 18.1368,
                "BYN" => 28.8985,
                "CAD" => 60.7392,
                "AZN" => 43.1216,
                "HKD" => 9.4436
              }}

    assert CurrencyConverter.get_sbr_daily(~D[2041-06-04]) == {:error, :incorrect_date}
  end
end
